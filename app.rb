#!ruby
# coding: utf-8

# App: 
# AppName:
# Abstruct: 
# Author: 
# Version: 

require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra-websocket'
require 'redis'
require 'redis-namespace'
require 'json'

configure do
  set :server, 'thin'
  set :bind, '0.0.0.0'
  set :erb, trim: '-'
  set :sockets, []
  set :redis, Redis::Namespace.new(:wschat, redis: Redis.new(host: "127.0.0.1", port: 6379))
end

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end

get '/' do
  if !request.websocket?
    erb :index
  else
    request.websocket do |ws|
      ws.onopen do
        settings.redis.lrange(:logs, -10, -1).each{|msg| ws.send(msg)}
        settings.sockets << ws
      end
      ws.onmessage do |data|
        msg = JSON.parse(data)
        if msg['opt'] == "ping"
          ws.send({msg: "", opt: "pong"}.to_json);
        else
          fmsg = data
          settings.redis.rpush :logs, fmsg
          EM.next_tick { settings.sockets.each{|s| s.send(fmsg) } }
        end
      end
      ws.onclose do
        settings.sockets.delete(ws)
      end
    end
  end
end

get '/clean' do
  settings.redis.del :logs
  redirect back
end

