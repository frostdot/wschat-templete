var ws = null;

function notification(text) {
    $('#notification').html('<div class="notice error">' + text + '<a href="#close" class="icon-remove"></a></div>');
}

function append_message(message) {
    $('#message').prepend('<div class="row"><div class="mes">' + message + '</div></div>');
}

function onOpen() {
    //notification('接続しました');
}

function onClose() {
    notification('切断されました。再接続中');
    setTimeout(connect, 3000);
}

function onMessage(msg) {
	var data = null;
    if (msg && msg.data){ 
		data = JSON.parse(msg.data);
		if (data.opt == "pong"){
			return;
		}
		append_message(data.msg);
	}
}

function ws_connect() {
    var s;
    s = new WebSocket('ws://' + window.location.host + '/');
    s.onopen = onOpen;
    s.onclose = onClose;
    s.onmessage = onMessage;
    return s;
}

function connect() {
    ws = ws_connect();
}

function ws_send(data) {
	if (ws != null){
		ws.send(JSON.stringify(data));
	}
}


$(function () {
    $('#form').submit(function (event) {
        event.preventDefault();
		var msg = $('#form [name=msg]').val();
		if (msg == ""){
			notification("内容が入力されていません");
			return;
		}
		var data = { msg: msg, opt: '' };
        ws_send(data);
        $('#form [name=msg]').val("");
    });

    setInterval(function () {
        ws_send( { user: '', msg: '', opt: 'ping'});
    }, 30000);
});

